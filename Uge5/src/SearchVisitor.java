/**
 * Created by Francis on 29-11-2014.
 */
public class SearchVisitor implements FileSystemVisitor<Void> {

	private String name;
	private String path;

	public SearchVisitor(String s) {
        name = s;
		path = "";
	}

	@Override
	public Void visitFile(FileNode f) {
		if (f.getName().contains(name)) {
			System.out.println(path + "/" + f.getName());
		}
		return null;
	}

	@Override
	public Void visitDirectory(DirectoryNode d) {
 		path = path + "/" + d.getName();

		for (FileSystemNode node : d) {
			node.accept(this);
		}
		return null;
	}
}
