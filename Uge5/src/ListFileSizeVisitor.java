/**
 * Created by Francis on 29-11-2014.
 */
public class ListFileSizeVisitor implements FileSystemVisitor<Integer> {
    private int depth;
	public ListFileSizeVisitor(){
            depth = 0;
    }
	@Override
	public Integer visitFile(FileNode f) {
        System.out.println(tab() + "File: " + f.getName() + " of "+f.getSize() + " bytes");
		return f.getSize();
	}

	@Override
	public Integer visitDirectory(DirectoryNode d) {
        System.out.println(tab() + "Directory " + d.getName() + " containing");
		depth = depth + 1;
		Integer sum = 0;
        for(FileSystemNode node : d){
            sum += node.accept(this);
		}
        depth = depth - 1;
		System.out.println(tab() + "Total: " + sum + " bytes");
		return sum;
	}
    public String tab(){
        String structure = "";
        for (int i = 0; i < depth; i++) {
            structure += "  ";
        }
        return structure;
    }
}
