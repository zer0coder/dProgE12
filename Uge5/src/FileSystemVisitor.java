/**
 * Created by Francis on 29-11-2014.
 */
public interface FileSystemVisitor<T> {
  public T visitFile(FileNode f);

  public T visitDirectory(DirectoryNode d);
}