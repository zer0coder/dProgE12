/**
 * Created by Francis on 02-12-2014.
 */
public class RecursiveMethod {
    public static void main(String[] args) {
        int x = 1;
        int n = 1;
        if(args.length==2) {
            x = Integer.parseInt(args[0]);
            n = Integer.parseInt(args[1]);
        }
        System.out.println(recursive(x,n));
    }
    private static int recursive(int x, int n) {
        int result = 0;
        if (n==0) {
            return 1;
        }
        if (n>0) {
            if((n/2)*2==n) {
                int tempResult = recursive(x,n/2);
                return tempResult*tempResult;
            }
            else
            {
                return recursive(x,n-1)*x;
            }
        }
        else {
            return result;
        }
    }
}
