package Test;

/**
 * Created by Francis on 29-11-2014.
 */
public interface FileSystemNode {
	public <T> T accept(FileSystemVisitor<T> v);
}