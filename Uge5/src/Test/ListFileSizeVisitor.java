package Test;

/**
 * Created by Francis on 29-11-2014.
 */
public class ListFileSizeVisitor implements FileSystemVisitor<String> {

	
	@Override
	public String visitFile(FileNode f) {
		String text = f.getName()+" of "+f.getSize() + " bytes"+"\n";
		return text;
	}

	@Override
	public String visitDirectory(DirectoryNode d) {
		String structure = "Directory " + d.getName() + " containing"+"\n";
		
		for(FileSystemNode node : d){
            structure += node.accept(this);
		}
		
		System.out.println(structure);
		return structure;
	}

}
