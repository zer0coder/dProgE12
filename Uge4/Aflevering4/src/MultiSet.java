import java.util.*;

/**
 * Created by Team NARH on 25-11-2014.
 */

/**
 * This class creates a set of X object which in this case are cards.
 * Futher it utilizes a HashMap to keep the objects.
 * @param <E> This parameter is the element of X object.
 */
public class MultiSet<E> extends AbstractCollection<E>{

    private HashMap<E,Integer> multiSet;
    private Set<E> set;

    public MultiSet() { startVar(); }

    /**
     * This allows an external class to add an object(s) to the MultiSet class.
     * @param c This parameter takes in objects to its collection.
     */
    public MultiSet(Collection<E> c) {
        startVar();
        addAll(c);
    }

    public void startVar() {
        multiSet = new HashMap<E, Integer>();
        set = multiSet.keySet();
    }

    public Iterator<E> iterator() {
        final Iterator<E> it = set.iterator();
        return new Iterator<E>() {
            E element;
            int pos;
            public boolean hasNext() {
                if (pos > 0) {
                    return true;
                }
                return it.hasNext();
            }
            public E next() {
                if (pos > 0) {
                    pos--;
                    return element;
                }
                element = it.next();
                pos = multiSet.get(element)-1;
                return element;
            }
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    @Override
    public String toString() { return multiSet.toString(); }

    @Override
    public int hashCode() { return multiSet.hashCode(); }

    @Override
    public boolean equals(Object o) {
        if(this == o) return true;
        if(o == null) return false;
        if(getClass() != o.getClass()) return false;

        @SuppressWarnings("unchecked")
        MultiSet<E> test = (MultiSet<E>) o;
        return multiSet.equals(test.returnHashMap());
    }

    @Override
    public boolean remove(Object o) {
        @SuppressWarnings("unchecked")
        E e = (E) o;
        if (!multiSet.containsKey(e)) {
            return false;
        }
        int i = multiSet.get(e);
        if(i > 1){
            multiSet.put(e, i-1);
        } else {
            multiSet.remove(e);
        }
        return true;
    }

    public HashMap<E, Integer> returnHashMap() { return multiSet; }

    @Override
    public int size() {
        int i = 0;
        for(E e : set) {
            i = i + multiSet.get(e);
        }
        return i;
    }

    @Override
    public boolean add(E e) {
        int val = 1;
        if(multiSet.containsKey(e)) {
            val = multiSet.get(e) + 1;
        }
        multiSet.put(e, val);
        return true;
    }
}
