import java.util.TreeSet;

/**
 * Created by Team NARH on 25-11-2014.
 */

/**
 * This class creates a hand that holds cards.
 */
public class Hand {
    private TreeSet<Card> tsCard = new TreeSet<Card>();

    public boolean add(Card c) { return tsCard.add(c); }
    public boolean remove(Card c) { return tsCard.remove(c); }

    @Override
    public boolean equals(Object o) {
        if(this == o) return true;
        if(o == null) return false;
        if(getClass() != o.getClass()) return false;
        Hand hand = (Hand) o;
        return hand.tsCard.equals(tsCard);
    }

    public int hashCode() {
        int i = 0;
        for(Card c : tsCard) {
            i += c.hashCode();
        }
        return i;
    }

    public String toString() {
        String format = "";
        for(Card c : tsCard) {
            format = format + " " + c.toString();
        }
        return format;
    }
}
