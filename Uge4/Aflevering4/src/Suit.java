/**
 * Created by Francis on 25-11-2014.
 */
public enum Suit {
    DIAMOND, CLUB, HEART, SPADE;
}
