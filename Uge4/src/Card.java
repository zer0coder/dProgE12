/**
 * Created by Francis on 25-11-2014.
 */
public class Card implements Comparable<Card> {


    private Suit sCard;
    private Rank rCard;

    public Card(Suit s, Rank r) {
        sCard = s;
        rCard = r;
    }

    public Suit getSuit() {
        return sCard;
    }

    public Rank getRank() {
        return rCard;
    }

    public int compareTo(Card card) {
        int i = rCard.compareTo(card.getRank());
        if(i == 0) {
            return sCard.compareTo(card.getSuit());
        }
        return i;
    }

    @Override
    public String toString() {
        String format = sCard.toString() + " " + rCard.toString();
        return format;
    }

    @Override
    public boolean equals(Object card) {
        if(card instanceof Card && sCard.equals(((Card) card).getSuit()))
            return rCard.equals(((Card) card).getRank());
        else
            return false;
    }

    @Override
    public int hashCode() {
        int x = 17;
        return sCard.hashCode()*(x*x)+sCard.hashCode()*x;
    }
}
