/**
 * Created by Francis on 02-12-2014.
 */
public class Binomial2 {
    public static void main(String[] args) {
        int n = 0;
        int k = 0;
        if(args.length==2) {
            n = Integer.parseInt(args[0]);
            k = Integer.parseInt(args[1]);
        }
        binomial(n,k);
    }
    public static long binomial(int n, int k) {
        int result = 0;
        if(n==k || k==0) {
            return 1;
        }
        if(n>k && k>0) {
            return binomial(n-1,k)+binomial(n-1,k-1);
        } else {
            return result;
        }
    }
}
