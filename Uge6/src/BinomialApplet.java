import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Francis on 08-12-2014.
 */
public class BinomialApplet extends JApplet{
    public void init() {
        setLayout(new GridLayout(2,3));
        add(new JLabel("Binomial Calculations"));

        final JLabel result = new JLabel();

        final JTextField n = new JTextField("1");
        final JTextField k = new JTextField("1");
        JButton B1 = new JButton("Calculate");
        final JCheckBox checkBox = new JCheckBox("Fast Method");

        add(n);
        add(k);
        add(B1);
        add(checkBox);
        add(result);

        B1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try{
                    long i = 0;
                    if(checkBox.isSelected()) {
                        i = Binomial2.binomial(Integer.parseInt(n.getText()), Integer.parseInt(k.getText()));
                    } else {
                        i = Binomial.binomial(Integer.parseInt(n.getText()), Integer.parseInt(k.getText()));
                    }
                    result.setText(String.valueOf(i));
                }
                catch (Exception e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(null,"An error has occured!","ERROR",JOptionPane.ERROR_MESSAGE);
                }
            }
        });

    }
}
