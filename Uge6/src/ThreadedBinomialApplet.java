import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Francis on 09-12-2014.
 */
public class ThreadedBinomialApplet extends JApplet{
    public void init() {
        setLayout(new GridLayout(2,3));
        add(new JLabel("Binomial Calculations"));

        final BinomialRunnable runnable = new BinomialRunnable();
        final JLabel result = new JLabel();

        runnable.setJ(result);

        final JTextField n = new JTextField("1");
        final JTextField k = new JTextField("1");
        final JCheckBox checkBox = new JCheckBox("Fast Method");

        JButton B1 = new JButton("Calculate");

        add(n);
        add(k);
        add(B1);
        add(checkBox);
        add(result);

        B1.addActionListener(new ActionListener() {
            Thread t = new Thread(runnable);
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try{
                    t.interrupt();

                    runnable.setK(Integer.parseInt(k.getText()));
                    runnable.setN(Integer.parseInt(n.getText()));
                    runnable.setFast(checkBox.isSelected());

                    t = new Thread(runnable);
                    t.start();
                }
                catch (Exception e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(null,"An error has occured!","ERROR",JOptionPane.ERROR_MESSAGE);
                }
            }
        });

    }
}
