import java.util.concurrent.LinkedBlockingQueue;
/**
 * Author:  Francisco
 * Project:    dProgE12
 * Date:    08-12-2014
 * Time:    10:20
 */

public class WordCountTest {
    public static void main(String[] args){

        final LinkedBlockingQueue<Integer> queue = new LinkedBlockingQueue<Integer>();
        String s ="";
        for(int i = 0; i<args.length; i++){
            s = args[args.length-i-1];
            WordCount wc = new WordCount(s, queue);
            Thread t = new Thread(wc);
            t.start();
        }

        int total = 0;
        int totalSum = 0;
        for(int i = 0; i<args.length; i++){
            try {
                total = queue.take();
                totalSum += total;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Total words counted: " + totalSum + " word(s).");
    }
}
