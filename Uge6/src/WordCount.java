import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;
/**
 * Author:  Francisco
 * Project:    dProgE12
 * Date:    06-12-2014
 * Time:    11:12
 */

public class WordCount implements Runnable {

    private Scanner scan;
    private File file;
    private int count;
    private LinkedBlockingQueue<Integer> lbq;

    public WordCount(String text, LinkedBlockingQueue<Integer> linked) {
        file = new File(text);
        this.lbq = linked;
    }

    @Override
    public void run() {
        try {
            scan = new Scanner(new FileInputStream(file));
            while (scan.hasNext()) {
                scan.next();
                count++;
            }
            System.out.println(file.getName() + ": " + count + " word(s).");
            lbq.add(count);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}

