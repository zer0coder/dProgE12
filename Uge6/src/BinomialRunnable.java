import javax.swing.*;

/**
 * Created by Francis on 09-12-2014.
 */
public class BinomialRunnable implements Runnable {
    public long binomial1(int n, int k) {
        if (!Thread.currentThread().isInterrupted()) {
            long result = 0;
            if (n == k || k == 0) {
                return 1;
            }
            if (n > k && k > 0) {
                return (binomial1(n, k - 1) * (n - k + 1)) / k;
            } else {
                return result;
            }
        }
        return 0;
    }

    public long binomial2(int n, int k) throws InterruptedException {
        if (!Thread.currentThread().isInterrupted()) {
            long result = 0;
            if (n == k || k == 0) {
                return 1;
            }
            if (n > k && k > 0) {
                return binomial2(n - 1, k) + binomial2(n - 1, k - 1);
            } else {
                return result;
            }
        }
        return 0;
    }

    private int n;
    private int k;
    private JLabel j;
    private boolean fast;

    public void setN(int n) { this.n = n; }
    public void setK(int k) { this.k = k; }
    public void setJ(JLabel j) { this.j = j; }
    public void setFast(boolean fast) { this.fast = fast; }


    @Override
    public void run() {
        try {
            long i = 0;
            if (fast) {
                i = binomial1(n, k);
            }
            else {
                i = binomial2(n, k);
            }
            if (!Thread.currentThread().isInterrupted()) {
                System.out.println(i);
                j.setText(Long.toString(i));
            }

        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
    }
}
