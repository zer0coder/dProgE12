import java.util.Scanner;

/**
 * A class for squaring a n amount of numbers.
 */
public class Square {
    /**
     * Our main asks for a certain n amount of numbers the user wants to square.
     * @param args is the parameter where the n number is entered.
     */
    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        int[] num = new int[n];
        Scanner in = new Scanner(System.in);
        for (int i = 0; i<n; i++) {
            System.out.print("Indtast tal nummer " + (i+1) + ": ");
            num[i] = in.nextInt();
            System.out.println();
        }
        System.out.println("Kvadraterne på de indtastede tal er: ");
        for (int i = 0; i<n; i++) {
            int squared = num[i] * num[i];
            System.out.print(" " + squared);
        }
    }
}
