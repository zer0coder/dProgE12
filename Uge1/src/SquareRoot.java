/**
 * Created with IntelliJ IDEA.
 * User: Francis
 * Date: 04-11-14
 * Time: 08:53
 * To change this template use File | Settings | File Templates.
 */
public class SquareRoot {
    public static int intSquareRoot(int n) {
        int a = n;
        int b = 1;
        while (a > b) {
            a = (a+b)/2;
            b = n/a;
        }
        assert (a*a <= n && n < (a+1)*(a+1));
        return a;
    }

    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        int r = intSquareRoot(n);
        System.out.println(r);
    }
}
