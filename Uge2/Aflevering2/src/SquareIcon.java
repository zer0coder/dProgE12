/**
 * Created by Francisco on 04-11-2014.
 */

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;

public class SquareIcon implements Icon {
    private int size;
    private Color color;

    /**
     * This constructor allows for custom inputs of the individual square icons.
     * @param size is the size of the icon.
     * @param color is the color of the icon.
     */
    public SquareIcon(int size, Color color) {
        this.size = size;
        this.color = color;
    }

    /**
     * Gets height
     * @return the height of the icon
     */
    public int getIconHeight() {
        return size;
    }

    /**
     * Gets the icon width
     * @return the width of the icon
     */
    public int getIconWidth() {
        return size;
    }

    /**
     * This constructor paints the individual icons contained in the composite.
     * @param c component
     * @param g graphics
     * @param x is the x-position of the most left icon.
     * @param y is the y-position
     */
    public void paintIcon(Component c, Graphics g, int x, int y) {
        Graphics2D g2 = (Graphics2D) g;
        Rectangle2D.Double square = new Rectangle2D.Double(x,y,size,size);
        g2.setColor(color);
        g2.fill(square);
    }
}
