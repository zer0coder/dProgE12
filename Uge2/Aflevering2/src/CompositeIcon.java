/**
 * Created by Francisco on 09-11-2014.
 */

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * This class is a composite of icons.
 */
public class CompositeIcon implements Icon {
    private int size;
    private Color color;
    private ArrayList<Icon> icn;

    /**
     * This constructor sets up a new ArrayList.
     */
    public CompositeIcon() {
        icn = new ArrayList<Icon>();
    }

    /**
     * This constructor enables the user to add icons to the pre-existing ArrayList
     * @param ic this parameter HAS to be an Icon.
     */
    public void addIcon(Icon ic) {
        icn.add(ic);
    }

    /**
     * Gets height
     * @return the height of the icon
     */
    public int getIconHeight() {
        return size;
    }

    /**
     * Gets the icon width
     * @return the width of the icon
     */
    public int getIconWidth() {
        int sum=0;
        for(Icon i : icn) {
            sum += i.getIconWidth();
        }
        return sum;
    }

    /**
     * This constructor paints the individual icons contained in the composite.
     * @param c component
     * @param g graphics
     * @param x is the x-position of the most left icon.
     * @param y is the y-position
     */
    public void paintIcon(Component c, Graphics g, int x, int y) {
        int relativePosition = 0;
        for(Icon i : icn) {
            i.paintIcon(c, g, x + relativePosition, y);
            relativePosition += i.getIconWidth();
        }
    }
}
