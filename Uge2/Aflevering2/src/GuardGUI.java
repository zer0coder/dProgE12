/**
 * Author:  TEAM NARH
 * Project:    dProgE12
 * Date:    12-11-2014
 * Time:    13:23
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * GuardGUI Class
 */
public class GuardGUI {

    /**
     * GuardGUI will create a frame that will allow the one to
     * tell the counter and RoadGUI when a car enters and change
     * the amount of space accordingly.
     */
    public GuardGUI() {
        JFrame frame = new JFrame("Aflevering 2 - P-Plads GUARD");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JButton carIn = new JButton("Car Enters");
        JButton carOut = new JButton("Car Exits");

        carIn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(RoadGUI.c.getValue() == 0) {
                    RoadGUI.c.change(0);
                } else {
                RoadGUI.c.change(-1); }
            }
        });
        carOut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(RoadGUI.c.getValue() == RoadGUI.MAX_SPACE) {
                    RoadGUI.c.change(0);
                } else {
                RoadGUI.c.change(1); }
            }
        });

        frame.setLayout(new FlowLayout());

        frame.add(carIn);
        frame.add(carOut);

        frame.pack();
        frame.setVisible(true);
    }

    /**
     * parkingControl allows control of whether the parking lot
     * is open or closed.
     */
    public void parkingControl() {
        JFrame cPanel = new JFrame("Aflevering 2 - P-Plads Status");
        cPanel.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JButton openPark = new JButton("Open Parking Lot");
        JButton closePark = new JButton("Close Parking Lot");

        openPark.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                RoadGUI.c.change(RoadGUI.MAX_SPACE);
            }
        });
        closePark.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                RoadGUI.c.change(-RoadGUI.c.getValue());
            }
        });

        cPanel.setLayout(new FlowLayout());

        cPanel.add(openPark);
        cPanel.add(closePark);

        cPanel.pack();
        cPanel.setVisible(true);
    }
}
