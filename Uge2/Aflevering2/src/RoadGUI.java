/**
 * Author:  Team NARH
 * Project:    dProgE12
 * Date:    12-11-2014
 * Time:    13:23
 */

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;

/**
 * Road GUIClass
 */
public class RoadGUI {
    private static JFrame statusFrame;
    private static int SPACE_LEFT;
    public static Counter c = new Counter(SPACE_LEFT);
    public static int MAX_SPACE;

    /**
     * RoadGUI creates a frame that contain information on
     * how many spaces there are in the parkinglot.
     * @param slots is the amount of space in the parking lot.
     */
    public void RoadGUI(int slots) {

        SPACE_LEFT = slots;
        MAX_SPACE = slots;
        c.change(SPACE_LEFT);

        JFrame frame = new JFrame("Aflevering 2 - P-Plads ROAD");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        final JLabel space = new JLabel(Integer.toString(c.getValue()));
        JLabel parkLabel = new JLabel(" - Total space left in parkinglot");

        c.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                space.setText(Integer.toString(c.getValue()));
            }
        });

        frame.setLayout(new FlowLayout());
        frame.add(space);
        frame.add(parkLabel);
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Similar to RoadGUI this frame will display the status of whether
     * the parking lot is open or closed.
     */
    public void parkingStatus() {
        statusFrame = new JFrame("Aflevering 2 - P-Plads Status");
        statusFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        final JLabel text = new JLabel("Parking Lot is: Open!");

        c.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if(c.getValue() <= 0) {
                    text.setText("Parking lot is: Closed!");
                }
                if(c.getValue() > MAX_SPACE) {
                    text.setText("You can't add more space to the parking lot!");
                }
                if(c.getValue() <= MAX_SPACE && c.getValue() > 0) {
                    text.setText("Parking lot is: Open!");
                }
            }
        });

        statusFrame.setLayout(new FlowLayout());
        statusFrame.add(text);
        statusFrame.pack();
        statusFrame.setVisible(true);
    }
}
