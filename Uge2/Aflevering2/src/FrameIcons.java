import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Class Description: This class will create a GUI that have three buttons each with
 * a different color. Each button press will create a icon with the corresponding color
 * assigned to each button.
 *
 * Author:  TEAM NARH
 * Project:    dProg2E12
 * Date:    07-11-2014
 * Time:    19:07
 */


public class FrameIcons {
    /**
     * Here we utilize an anonymous class for CBox since it is used three times.
     * No other pattern is used besides Composite.
     */
    private static CompositeIcon CBox = new CompositeIcon();
    private static JFrame frame;
    private static JLabel label = new JLabel(" ",CBox,JLabel.CENTER);

    /**
     * The main method here will draw a GUI in Java, where three buttons
     * will allow the user to draw the corresponding square to the right side
     * of the buttons.
     * @param args
     * There are no need to input arguments to args.
     * The JFrame interface is utilized to create the GUI.
     */
    public static void main(String[] args) {
        frame = new JFrame("Aflevering 2");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Icon red = new SquareIcon(20, Color.RED);
        Icon green = new SquareIcon(20,Color.GREEN);
        Icon blue = new SquareIcon(20,Color.BLUE);

        JButton redButton = new JButton("Rød",red);
        JButton greenButton = new JButton("Grøn",green);
        JButton blueButton = new JButton("Blå",blue);

        redButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                CBox.addIcon(new SquareIcon(20, Color.RED));
                frame.pack();
                frame.repaint();
            }
        });
        greenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                CBox.addIcon(new SquareIcon(20, Color.GREEN));
                frame.pack();
                frame.repaint();
            }
        });
        blueButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                CBox.addIcon(new SquareIcon(20, Color.BLUE));
                frame.pack();
                frame.repaint();
            }
        });

        frame.setLayout(new FlowLayout());

        frame.add(redButton);
        frame.add(greenButton);
        frame.add(blueButton);
        frame.add(label);

        frame.pack();
        frame.setVisible(true);
    }
}
