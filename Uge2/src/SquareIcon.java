/**
 * Created by Francisco on 04-11-2014.
 */
import java.awt.*;
import java.awt.geom.*;
import javax.swing.*;

public class SquareIcon implements Icon {
    private int size;
    private Color color;
    public SquareIcon(int size, Color color) {
        this.size = size;
        this.color = color;
    }

    public int getIconHeight() {
        return size;
    }
    public int getIconWidth() {
        return size;
    }

    public void paintIcon(Component c, Graphics g, int x, int y) {
        Graphics2D g2 = (Graphics2D) g;
        Rectangle2D.Double square = new Rectangle2D.Double(x,y,size,size);
        g2.setColor(color);
        g2.fill(square);
    }
}
