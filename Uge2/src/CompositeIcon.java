/**
 * Created by Francisco on 09-11-2014.
 */
import java.awt.*;
import java.awt.geom.*;
import javax.swing.*;
import java.util.ArrayList;

public class CompositeIcon implements Icon {
    private int size;
    private Color color;
    private ArrayList<Icon> icn;

    public CompositeIcon() {
        icn = new ArrayList<Icon>();
    }

    public void addIcon(Icon ic) {
        icn.add(ic);
    }

    public int getIconHeight() {
        return this.size;
    }
    public int getIconWidth() {
        int sum=0;
        for(Icon i : icn) {
            sum += i.getIconWidth();
        }
        return sum;
    }

    public void paintIcon(Component c, Graphics g, int x, int y) {
        int relativePosition = 0;
        for(Icon i : icn) {
            i.paintIcon(c, g, x + relativePosition, y);
            relativePosition += i.getIconWidth();
        }
    }
}
