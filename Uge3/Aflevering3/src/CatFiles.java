import java.io.*;

/**
 * Author:  Team NARH
 * Project:    dProgE12
 * Date:    14-11-2014
 * Time:    21:36
 */

/**
 * This is the CatFiles class that merges multiple text files together.
 */
public class CatFiles {

    /**
     * This constructor takes user-defined inputs for which text files
     * that should be merged to a singular file
     * @param args the arguments/parameters has to be text files with
     *             the file extension (ie. file.txt).
     */
    public static void main(String[] args) {
        String[] file = args;
        int x = 0;
        int n = args.length-1;

        if(n<0) throw new NegativeArraySizeException("CatFiles require at least ONE argument to proper function. Two is recommended.");

        File[] mfiles = new File[n];
        File mergedFiles = new File(file[n]);
        while(x < n) {
            mfiles[x] = new File(file[x]);
            x++;
        }
        mergedFiles(mfiles, mergedFiles);
    }

    /**
     * Similar to main class, this is takes the multiple files and
     * designated merge-file and writes the text from the other files
     * into them merger.
     * @param mfiles the files selected.
     * @param mergedFile the merged file.
     */
    public static void mergedFiles(File[] mfiles, File mergedFile) {

        FileWriter fstream;
        BufferedWriter outStream = null;
        try {
            fstream = new FileWriter(mergedFile, true);
            outStream = new BufferedWriter(fstream);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        for (File f : mfiles) {
            System.out.println("Merging: " + f.getName() + " to: " + mergedFile.getName());
            FileInputStream fileInStream;
            try {
                fileInStream = new FileInputStream(f);
                BufferedReader in = new BufferedReader(new InputStreamReader(fileInStream));

                String aLine;
                while ((aLine = in.readLine()) != null) {
                    outStream.write(aLine);
                    outStream.newLine();
                }

                in.close();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }

        try {
            outStream.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }
}
