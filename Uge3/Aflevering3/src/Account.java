/**
 * Author:  Francisco
 * Project:    dProgE12
 * Date:    14-11-2014
 * Time:    21:29
 */
public interface Account {
    public void deposit(int amount);

    public boolean withdraw(int amount);

    public int getBalance();

    public void yearEnd();
}
