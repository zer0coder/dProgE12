import javax.swing.*;
import java.util.Scanner;
import java.io.*;

/**
 * Author:  Francisco
 * Project:    dProgE12
 * Date:    14-11-2014
 * Time:    21:01
 */
public class CopyFile {
    private static String[] files;
    public static void main(String[] args) throws Exception {

        files = args;

        File file = new File(files[0]);
        Scanner scan = new Scanner(file);

        while (scan.hasNextLine()) {
            String text = scan.nextLine();
            writeText(text);
        }
        scan.close();
    }

    public static void writeText(String copyText) throws Exception {

        String newLine = System.getProperty("line.separator");

        Writer output = null;
        File file = new File(files[1]);

        if(files[0].equals(files[1])) {
            JOptionPane.showMessageDialog(
                    null,
                    "It is not possible to write " + files[0] + " to itself!",
                    "Detected self-copying",
                    JOptionPane.ERROR_MESSAGE);
            output.close();
            System.exit(0);
        }

        output = new BufferedWriter(new FileWriter(file, true));
        output.write(copyText);
        output.write(newLine);
        output.close();
    }
}
