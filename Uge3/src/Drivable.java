public interface Drivable {
    public boolean drive(int distance);
    public double getMileage();
}
